/**
 * HobbiesController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const mail = require('../services/email')
module.exports = {
    placeHobby: function(req, res) {
        const accountSid = 'AC57c712bc73a63301aa17ee3c07a0d008';
        const authToken = '6a8d3c3d344102df69a12fe54ce8ea1d';
        const client = require('twilio')(accountSid, authToken);
        console.log(req.body)
        var allowedParameters = [
            "hobby", "userid",
        ]
        var data = _.pick(req.body.hobbbby, allowedParameters)
        
        console.log(data);
        
        Hobbies.create(data, function (err, createdData) {
            if (err) {
                return res.send({
                    success: false,
                    message: err.message,
                });
            } else {
                // mail("You have added a new hobby");
                // client.messages
                //     .create({
                //         body: `You have added a new hobby`,
                //         from: '+12676680349',
                //         to: '+2348149916606'
                //     })
                //     .then(message => console.log(message.sid))
                //     .done();
                return res.json({
                    data: createdData
                });
                
            }
        });
    },

    getHobby: async function (req, res) {
        var allowedParameters = [
            "userid"
        ]
        var data = _.pick(req.body, allowedParameters);
        var gottenHobbies = await Hobbies.find({ where: { userid: data.userid}, select : ['hobby']  });
        return res.send({
            message: "Hello",
            gottenHobbies
        });
    },
};

